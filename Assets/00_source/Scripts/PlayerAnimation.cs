﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : CharacterAnimation
{
    private bool hasWeapon;

    override protected void Start()
    {
        base.Start();
        EquipmentManager.instance.onEquipmentChangedCallback += OnEquipmentChanged;
    }

    override protected void OnAttack()
    {
        if (hasWeapon)
        {
            animator.SetInteger("AttackIndex", Random.Range(1, 3));
        }
        else
        {
            animator.SetInteger("AttackIndex", 0);
        }

        base.OnAttack();
    }

    private void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if (newItem != null && newItem.equipSlot == EquipmentSlot.Weapon) hasWeapon = true;
        else if (oldItem != null && oldItem.equipSlot == EquipmentSlot.Weapon) hasWeapon = false;
    }
}
