﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimation : MonoBehaviour
{
    protected Animator animator;
    NavMeshAgent agent;

    protected virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        var combat = GetComponent<CharacterCombat>();
        if (combat != null) combat.OnAttack += OnAttack;
    }

    void Update()
    {
        float speed = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("Speed", speed);
    }

    protected virtual void OnAttack()
    {
        animator.SetTrigger("Attack");
    }
}
