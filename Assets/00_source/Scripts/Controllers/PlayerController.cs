﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CharacterMotor))]
public class PlayerController : MonoBehaviour
{
    public LayerMask groundMask;

    private CharacterMotor motor;

    private Interactable focus;

    void Start()
    {
        motor = GetComponent<CharacterMotor>();
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;

        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f, groundMask))
            {
                RemoveFocus();
                motor.MoveToPoint(hit.point);
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f))
            {
                var interactable = hit.collider.GetComponent<Interactable>();
                if (interactable != null)
                {
                    SetFocus(interactable);
                }
            }
        }
    }

    void SetFocus(Interactable newFocus)
    {
        if (focus != newFocus)
        {
            if (focus != null) focus.OnDefocused();
            focus = newFocus;
            newFocus.OnFocused(transform);
            motor.FollowTarget(newFocus);
        }
    }

    void RemoveFocus()
    {
        if (focus != null) focus.OnDefocused();
        focus = null;
        motor.StopFollowingTarget();
    }
}
