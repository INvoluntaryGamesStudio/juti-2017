﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    public int baseValue;

    public List<int> modifiers = new List<int>();

    public void AddModifier(int value)
    {
        modifiers.Add(value);
    }

    public void RemoveModifier(int value)
    {
        modifiers.Remove(value);
    }

    public int GetValue()
    {
        int total = baseValue;
        for (int i = 0; i < modifiers.Count; i++) total += modifiers[i];
        return total;
    }
}
