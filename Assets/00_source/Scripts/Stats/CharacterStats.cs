﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int curretHealth { get; private set; }

    public Stat damage;
    public Stat armor;

    void Awake()
    {
        curretHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Max(damage, 0);

        curretHealth -= damage;
        Debug.Log(gameObject.name + " takes " + damage + " damage.");

        if (curretHealth <= 0) Die();
    }

    public virtual void Die()
    {
        Debug.Log(gameObject.name + " died.");
    }
}
