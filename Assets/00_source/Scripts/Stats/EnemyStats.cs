﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : CharacterStats
{
    override public void Die()
    {
		base.Die();
        Destroy(gameObject);
    }
}
