﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Items/Create New Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";

    public bool isDefaultItem;

    public Sprite icon;

    public virtual void Use()
    {
        Debug.Log("Using item " + name);
    }
    
	public void RemoveFromInventory ()
	{
		Inventory.instance.Remove(this);
	}
}
