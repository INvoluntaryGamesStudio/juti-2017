﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    public static EquipmentManager instance;

    void Awake()
    {
        instance = this;
    }

    public Equipment[] defaultEquipment;
    public SkinnedMeshRenderer targetMesh;

    private Equipment[] currentEquipment;
    private SkinnedMeshRenderer[] currentMeshes;

    private Inventory inventory;

    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChangedCallback;

    void Start()
    {
        inventory = Inventory.instance;

        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
        currentMeshes = new SkinnedMeshRenderer[numSlots];

        if (targetMesh == null) targetMesh = PlayerManager.instance.player.GetComponentInChildren<SkinnedMeshRenderer>();

        EquipDefaults();
    }

    public void Equip(Equipment newItem)
    {
        int slotIndex = (int)newItem.equipSlot;

        var oldItem = Unequip(slotIndex);

        currentEquipment[slotIndex] = newItem;

        if (onEquipmentChangedCallback != null) onEquipmentChangedCallback.Invoke(newItem, oldItem);

        AttachToMesh(newItem, slotIndex);
    }

    public Equipment Unequip(int slotIndex)
    {
        Equipment oldItem = currentEquipment[slotIndex];
        if (oldItem != null)
        {
            inventory.Add(oldItem);

            if (currentMeshes[slotIndex] != null)
            {
                Destroy(currentMeshes[slotIndex].gameObject);
            }

            currentEquipment[slotIndex] = null;

            if (onEquipmentChangedCallback != null) onEquipmentChangedCallback.Invoke(null, oldItem);
        }
        return oldItem;
    }

    public void UnequipAll()
    {
        for (int i = 0; i < currentEquipment.Length; i++) Unequip(i);

        EquipDefaults();
    }

    private void AttachToMesh(Equipment newItem, int slotIndex)
    {
        SkinnedMeshRenderer newMesh = Instantiate(newItem.mesh) as SkinnedMeshRenderer;
        newMesh.transform.parent = targetMesh.transform.parent;

        newMesh.rootBone = targetMesh.rootBone;
        newMesh.bones = targetMesh.bones;

        currentMeshes[slotIndex] = newMesh;
    }

    void EquipDefaults()
    {
        foreach (Equipment e in defaultEquipment)
        {
            Equip(e);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U)) UnequipAll();
    }
}
