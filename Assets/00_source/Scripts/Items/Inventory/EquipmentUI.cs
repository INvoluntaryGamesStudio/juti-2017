﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentUI : MonoBehaviour
{
    private EquipmentManager equimpent;

    EquipmentSlotUI[] slots;

    void Start()
    {
        equimpent = EquipmentManager.instance;
        slots = GetComponentsInChildren<EquipmentSlotUI>();

        equimpent.onEquipmentChangedCallback += OnEquipmentChanged;
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        int index = 0;
        if (oldItem != null) index = (int)oldItem.equipSlot;
        else if (newItem != null) index = (int)newItem.equipSlot;

        if (newItem != null && !newItem.isDefaultItem)
        {
            slots[index].SetItem(newItem);
        }
        else
        {
            slots[index].ClearSlot();
        }
    }
}
