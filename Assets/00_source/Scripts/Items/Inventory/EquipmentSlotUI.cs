﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentSlotUI : MonoBehaviour
{
    public Image icon;

    private Equipment item;

    public void SetItem(Equipment newItem)
    {
        item = newItem;

        icon.sprite = newItem.icon;
        icon.enabled = true;
    }

    public void ClearSlot()
    {
        item = null;

        icon.sprite = null;
        icon.enabled = false;
    }

    public void Unequip()
    {
        if (item != null)
        {
            EquipmentManager.instance.Unequip((int)item.equipSlot);
        }
    }
}
