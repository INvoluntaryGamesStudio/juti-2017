﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Interactable
{
    private CharacterStats myStats;

    void Start()
    {
        myStats = GetComponent<CharacterStats>();
    }

    override public void Interact()
    {
        base.Interact();
        CharacterCombat playerCombat = PlayerManager.instance.player.GetComponent<CharacterCombat>();
        if (playerCombat != null)
        {
            playerCombat.Attack(myStats);
        }
		hasInteracted = false;
    }
}
