﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : Interactable
{
    public Item item;

    override public void Interact()
    {
        base.Interact();
        PickUp();
    }

    void PickUp()
    {
        Debug.Log("Picked item " + item.name);
        if (Inventory.instance.Add(item))
            Destroy(gameObject);
    }
}
