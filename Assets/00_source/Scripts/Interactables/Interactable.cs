﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius;
    public Transform interactionTransform;

    private Transform focuser;
    private bool isFocus;
    protected bool hasInteracted;

    public virtual void Interact()
    {
        // Debug.Log("Interacting with " + gameObject.name);
    }

    public void OnFocused(Transform newFocuser)
    {
        isFocus = true;
        hasInteracted = false;
        focuser = newFocuser;
    }

    public void OnDefocused()
    {
        isFocus = false;
        hasInteracted = false;
        focuser = null;
    }

    void Update()
    {
        if (isFocus && !hasInteracted)
        {
            var dist = Vector3.Distance(interactionTransform.position, focuser.position);
            if (dist <= radius)
            {
                hasInteracted = true;
                Interact();
            }
        }
    }

    void OnDrawGizmos()
    {
        if (interactionTransform == null) interactionTransform = transform;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
